package j.assessment;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import j.assessment.CurrencyPricing;

public class CurrencyPricingTesting {

	
	/**
	 * testing converting from USD TO DKK
	 * actual value from CSV : USD,BZD,1.9368
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void majorMinorTest() {
		String filename = "rates.csv";
		try {
			CurrencyPricing cp = new CurrencyPricing(filename);
			Assert.assertTrue(1.9368 == cp.getFXQuote("USD","BZD"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("file not found exception");
		}
	}
	
	/**
	 * testing converting from DKK TO DOP
	 * actual value  : DKK,DOP,0.15055033991583036
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void minorMinorTest() {
		String filename = "rates.csv";
		try {
			CurrencyPricing cp = new CurrencyPricing(filename);
			Assert.assertTrue(0.15055033991583036 == cp.getFXQuote("DKK","DOP"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("file not found exception");
		}
	}

}
