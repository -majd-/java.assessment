package j.assessment;


/**
 * Author Majd Asab
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class CurrencyPricing {

	// Map to store c1c2=double
	Map<String,String> mapVals = new HashMap<String,String>();
	
	/**
	 * Class constructor to read CSV file.
	 * File must be placed under the top level project structure.
	 * @param fileName
	 * @throws FileNotFoundException
	 */
	public CurrencyPricing(String fileName ) throws FileNotFoundException {
		Scanner sc = new Scanner(new File("./"+fileName));
		sc.useDelimiter("\n");
		while(sc.hasNext()) {
				String[] info =  sc.next().split(",");
				String combo = info[0] + info[1];
				mapVals.put(combo, info[2]);

		}
		sc.close();
	}
	
	/**
	 * Method to find the currency rate.
	 * @param c1
	 * @param c2
	 * @return exchange rate : double
	 */
	public double getFXQuote(String c1, String c2) {
		if(mapVals.get(c1+c2) != null) {
			return Double.parseDouble(mapVals.get(c1+c2));
		}else {
			// find USD/c1
			double usdc1 = Double.parseDouble(mapVals.get("USD"+c1));
			// find USD/c2
			double usdc2 = Double.parseDouble(mapVals.get("USD"+c2));
			
			// final value 
			return usdc1/usdc2;
		}
		
	}
	
	public static void main(String[] args) {
		String filename = "rates.csv";
		
		try {
			CurrencyPricing cp = new CurrencyPricing(filename);
			System.out.println(cp.getFXQuote("DKK", "DOP"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
